# SWCTN Automation Prototypes

## Questions

### Project name *

GitBuilding ? / BuildUp ?  / BuildInstructions.com / Next Assembly ?

### Project summary (200 character limit) *

Software tools for authoring beautiful and accessible do-it-yourself instructions that let people work better with manufacturing automation and focus on building.


### Tell us about your idea. Please respond to the interests outlined under 'The Theme: what are we looking to fund in automation?' indicating how you are responding to our priorities (2500 character limit) *

Building a more desirable future requires building things, useful, robust and repairable things. It requires that we not silo the knowledge of how to build things and to make it easier to share.

We need to give people the ability to customise existing hardware to their needs and good documentation is part of that. We also need to make it more efficient for people to come up with designs that solve their own problems. Automation can play a big role in making all of this easier for people.

We are developing tools that help people document how to build things. Our initial prototype, called GitBuilding, was developed by researchers in the Open Instrumentation Group at the University of Bath. The group's flagship design is a microscope with a precision moveable stage (openflexure.org). This device gives you the capabilities of an instrument that is worth thousands of pounds but costs a fraction of that. The catch: you have to build it yourself. You have to figure out which parts need to be 3D-printed, put together a shopping list of all the off-the-shelf components (nuts, bolts, optics, motors, electronics) and find out where to obtain each of them and manually place an order.

We seek to automate of this what can be and empower people to build things themselves. GitBuilding is a user-friendly editor that lets you generate parts and tool lists from written instructions: letting people write and document and computers count and collate in the background. We want to develop this into a second prototype and allow easier, independant ordering of these parts lists: click on a button and buy everything you need. Not from us, or from the author, but for yourself from various online distributors.

### Explain how your prototype has commercial potential and possible social impact (2500 character limit) *

This is not an unproven model. Kaspar created and maintains Kitspace.org which automates parts purchasing for electronics designs in a similar way. This technology is currently on the path to sustainable commercialisation through Kitspace Manufacturing Software Ltd. This is still a startup business but it does show a potential avenue for commercialisation.

The central thesis to the business model is that while big corporations have processes for procurement automation, through complicated and expensive enterprise resource planning software systems, the market is overlooking makers, scientist, artists, humanitarian workers, contractors and small batch assemblers that can also benefit from similar automation but need a more decentralised system.

Part of the inspiration for developing the initial prototype is that we can see a need for this outside of scientific instrumentation. We are part of the working group for the Open Know How standard which was spear-headed by people from Field Ready.

Field Ready is an NGO that takes rapid prototyping equipment such as 3D printers out to disaster struck areas to collaborate with locals and re-build critical infrastructure quickly. One of the most successful designs that has emerged is a 3D printed pipe fitting adapter, that can be printed in the correct size depending on the local system, to be used to repair fresh water networks. Another design requires much more complicated assembly before it can be used: a heavy lifting air-bag that is able to lift rubble for rescue operations.

Field Ready have a multitude of designs like this and are looking for a way and platform to document them well and share them. So far they have failed to find one. We want to work with them and help them and make sure we make something for their use-case in mind.

Aside from such obviously socially beneficial use-cases there are more subtle ones. Making things is therapeutic

### Tell us who is in your team and why we should invest in you (1500 character limit) *

Kaspar Emanuel Bumke is the founder of Kitspace. He is an electronic engineer and software developer who works part-time as a 3D printing and instrumentation technician at the University of Bath. Here he works alongside Julian Stirling who is a post-doctoral researcher and came up with the idea for GitBuilding and programmed the first version. Jon is a maker and engineer but most importantly a user experience and user interaction designer and a co-founder of Kitspace.

### Describe what impact your prototype will have in the South West (1500 character limit) *

### List the areas of interest and industry/sectors that your team brings together (500 character limit) *

Software Engineering, Digital design (UX/UI), Open Science, Open Research, New Media Art, Open Source Hardware, ??Humanitarian Making.

### Demonstrate where diversity exists in your proposal (1500 character limit)

As a team consisting completely of white people and mostly of men we are bound to have blind spots. We need to mitigate this by carrying out user research among groups that are more representative of the population of the South West.

### Project plan; Upload a project plan (1 A4 page only). Please save as - 'yourname'Plan *

### Budget; Upload a spreadsheet with an overview budget. Please save as 'yourname'Budget *
### Optional: A link to a supporting document (max 2 A4 pages) or video (max 3 minutes). This is an opportunity to communicate your idea visually.

## Appendix

### Description

**The opportunity:**
* There is currently no easy or standard way to collaborate, define and automate ordering parts for open source hardware projects. The existing 'solutions' are usually just blog-like sites with functionality that goes only a little way to making it easier to document and automate project assembly.
* This creates barriers for entry to the potential users/audience and inhibits the spread of new sustainable ideas and local manufacturing
* We envision a world with less waste, less energy used on transporting goods and increased innovation through open, sharing practices.

**Our solution:**
* **BuildUp** Open standard for defining physical project documentation *Status: 50% complete*
This will be an open standard freely available for anybody to use and integrate into their project or tool
* **GitBuilding** Open platform for collaborating on physical project documentation*Status: 50% complete*
A beautiful open platform for Artists and Scientists to collaborate and share their project documentation ([link](https://gitlab.com/bath_open_instrumentation_group/git-building))
* **B-O-M Orderer** Parts selection and ordering automation In-progress: *Status: 50% complete*
An API for end users to order parts to build their open source project


On the front end: A software tool that makes it easier to create beautiful build instructions. The tool generates parts lists from freely written instructions and allows people to easily buy parts through browser automation.
Powering it: An open standard for participatory design for local communities and open, transparent innovation.

**This is:**
* Design automation for makers, artists, scientists working on “DIY” projects
* A perfect complement to desktop digital manufacturing automation such as 3D printing: print you parts, then follow the instructions to assemble them into a working device
* Purchasing automation directed at individuals rather than large organisations


**Our mission:**
* We want to make it easier for artists and scientists to document their open source hardware projects, users to build these projects improve the documentation and make it easier to collaborate.
* We believe this will help improve  to the creative commons for open, sustainable hardware. This hardware is a much for medical and humanitarian applications among others.

### Diversity

* We are democratising art and engineering for everybody to play and experiment with. Lowering the cost, bringing resilience and innovation to communities and reducing the impact of climate change through sustainable, repairable and resilient designs.
* The instructions format will be easily shareable and open to anybody
* The software presentation layer will be both beautiful and meet accessibility standards so that anybody can use it
* The tool will reduce the cost of accessing innovation and knowledge
* The tool will break down some of the barriers between academia and knowledge-transfer

### Impact

* We want to make the southwest a local manufacturing hub and centre for open innovation, where we build for our local needs together. Reducing transport pollution bringing in goods and services and solving our own problems locally with open innovation.
* This will create new entrepreneurial opportunities for local small-scale manufacturers, artists, engineers and scientists.

### Stories

Marley steps out of a car in a landscape of waterlogged and crumbling buildings. Under one arm they carry a state of the art 3D printer under the other a laptop with an integrated mobile network adapter. Marley is a volunteer with the aide organisation Field Ready and has come here in response to a recent tsunami. Field Ready specialises in working with locals in disaster struck areas to design and manufacture solutions for the most pressing issues and rebuild life-supporting infrastructure.

Caden, a local here, has been attempting to repair the fresh water supply to the tap in the village center. The piping has burst in several locations and a few stretches of it were completely swept away by the floods. Marley notes that these are non-standard fittings and they require adapters. They are able to find the correct parametric model, modify it to their needs and print several dozen. Using GitBuilding they are quickly able to draft instructions for volunteers to assemble them and put them to use and to document the design adaptation for future use.

At the other end of the world Kelsey is frustrated because their stereo has stopped working.


